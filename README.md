**There are four serial codes of NPT Monte Carlo simulation of charged colloids with hard-core repulsive Yukawa potentail, written in C.**

In this code, I calculate the phase diagram of the studied system using Kofke Integration.

Ref1: M. Dijkstra et al., PRE 68, 021407 (2003).
   
 	 

 *	 Compile code using: 
 *	 	 gcc filename.c -lm

      



 *	 Run :
 *		 nohup ./a.out &


![PhaseDiagramE8Colloids](/uploads/20e5b1a498b436faa9d2a5f9d49e1aac/PhaseDiagramE8Colloids.png)

Phase diagram for a system in which the particles interact via a
hard-core repulsive Yukawa pair potential with β\epsilon = 8. Solid line are coexistence lines obtained from my simulation and
blue points corresponds to the data taken from Ref1.

     
 Written By::<br/>
       **Rajneesh Kumar**
 *	Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,
 *	Bengaluru 560064, India.
 *	Email: rajneesh[at]jncasr.ac.in
 *	27 Dec, 2020

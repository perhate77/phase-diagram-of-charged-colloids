#define NDIM  3

#include "in_mddefs.h"
#include "gasdev.h"
#include "ran2.h"
#include "gamdev.h"
#include "maxboltzdev.h"
typedef struct {
	VecR r, rv, ra;
	//VecR rOld, rvOld, rFlag, mfpt;
} Mol;

void PrintChainProps (FILE *);	//do print the required value to the file.
void PrintChainProps1 (FILE *);
void PrintChainConfig (FILE *);
void PrintPropsToFile (FILE *);
void PrintPropsGrToFile (FILE *);
void PrintPropsSTDOUT ();
void monteCarloNPT ( );
void monteCarloMove ( );
void monteCarloVol ( );
int potEnergy ( );
int potEnergyPerAtom (int);
void CalcGr ( );
void ApplyPeriodicBoundaryCond ();

void monteCarloNPTFcc ( );
void monteCarloMoveFcc ( );
void monteCarloVolFcc ( );
int potEnergyFcc ( );
int potEnergyPerAtomFcc (int);
void ApplyPeriodicBoundaryCondFcc ();
void InitCoordsFcc ();


Mol *mol, *molFcc;
int c;
VecR region, vSum;
real PI, length, rho, lengthFcc, rhoFcc, temperature, pressure, beta, rCut, rCutFcc, ri3Cut, rrCut, rrCutFcc, rri, rri3, vir, virFcc, pSum, pSumFcc, rhoSum, rhoSumFcc, rhoSumFcc1, rhoSum1, vol, deltalnV, deltaR, deltalnVFcc, deltaRFcc, pCorr, pCorrFcc, energy, energyFcc, eSum, eSumFcc, eCorr, eCorrFcc, DkeCorr, DkeCorrFcc, eCut, simulatedPress, epsilon, kD, kD1, phi, phiFcc;
int moreCycles, nMol, stepAvg, stepCount, stepEquil, stepLimit, sampleCount, sigma;
VecI cells;
int acc, nAcc, nTransAcc, nVolAcc, nTransAtt, nVolAtt;
int accFcc, nAccFcc, nTransAccFcc, nVolAccFcc, nTransAttFcc, nVolAttFcc;
int molIndex, Ucell;
long seed, seedFcc, initSeed;
real *gr, *grArray, binWidth, rCutGr, rrCutGr, rCutHS, rrCutHS, consttFact, rIndex, grpTerm, grPressure, rangeGr;
int nHist, ngr, limitGr, Flag;
FILE *fp, *fp1, *fp2, *fp3, *fp4;
real kDInv, deltakDInv, deltakD, deltaPress, DkEn, DkEnFcc;
char infile1[500], infile2[500], outfile[500], infile3[500];




/*NameList nameList[] = {
 * NameI (stepAvg),
 * NameI (stepEquil),
 * NameI (stepLimit),
 * NameR (temperature),
 * NameR (bondLim),
 * NameI (chainLen),
 * NameR (MDdeltaT),
 * NameR (fricStat),
 * NameR (springConst),
 * };*/


//#####################################
int main (int argc, char **argv)
{ 
	nMol = 256;
	AllocArrays ();
	fp = fopen ("TestInvKDWithOutCorrKofkeIntFluidFccOutput_T_1.0_P_44.1_E_8_K_Colloids.dat", "w");
	//fp3 = fopen ("fcc.dat", "w");
	deltakDInv = 0.004;
	//deltakD = 1. / deltakDInv;
	deltaPress = 0.;
	pressure = 44.1;
	for (kDInv = 0.286; kDInv <= 0.5; kDInv += deltakDInv) {
		
		
		SetParams ();
		SetupJob ();
		
		//PrintChainConfig (fp3);
		
		moreCycles = 1;
		while (moreCycles) {
			SingleStep ();
			if (stepCount >= stepLimit)	moreCycles = 0;
		}
		
		//eSum /= (sampleCount * nMol);
		rhoSum /= sampleCount;
		rhoSumFcc /= sampleCount;
		rhoSum1 /= sampleCount;
		rhoSumFcc1 /= sampleCount;
		eSum /= (sampleCount * nMol);
		eSumFcc /= (sampleCount * nMol);
		
		kD1 = 1. / (kDInv + deltakDInv);
		deltakD = -1. * (kD - kD1);
		
		deltaPress = -1. * ((eSum - eSumFcc) / (rhoSum1 - rhoSumFcc1)) * deltakD;
		phi = (PI / 6.) * rhoSum;
		phiFcc = (PI / 6.) * rhoSumFcc;
		//simulatedPress = (pSum / sampleCount) + (rhoSum * temperature);
		
		//grPressure = temperature * rhoSum * ((2.* PI * rhoSum * pow(rCutHS,3) * grpTerm) / 3.);
		//printf("%lf \t\t %lf \t\t %lf \t\t %lf \t\t %lf \t\t %lf \t\t %lf\n",kDInv, kD, pressure, rhoSum, rhoSumFcc, phi, phiFcc);
		PrintPropsToFile (fp);
	}
	//PrintPropsSTDOUT ();
	fclose (fp);
	//fclose (fp3);
	return 0;
}


//#####################################
void SingleStep ()
{
	int n;
	++ stepCount;
	//printf("%d\t\t%lf\t\t%lf\n",stepCount,rho, rhoFcc);
	monteCarloNPT ();
	monteCarloNPTFcc ();
	if (stepCount >= stepEquil && (stepCount - stepEquil) % 1 == 0) {
		//CalcGr ();
		EvalProps ();
		++ sampleCount;
	}
}


//#####################################
void SetParams ()
{
	//nMol = 256;
	stepAvg = 1;
	stepEquil = 30000;
	stepLimit = 60000;
	temperature = 1.;
	pressure += deltaPress;
	rho = 0.6;
	deltaR = 0.1;
	deltalnV = 0.005;
	rhoFcc = 1.;
	deltaRFcc = 0.1;
	deltalnVFcc = 0.005;
	epsilon = 8.;
	kD = 1. / kDInv;    
	
	nHist = 3000;
	rangeGr = 3.;
	limitGr = stepLimit;
	binWidth = 0.001;//(rangeGr) / (nHist);
	
	PI = 3.14159265358979;
	consttFact = (4. / 3.) * PI;
	//seed = -98765432;
	initSeed = -98765432;
	
	seed = initSeed;
	seedFcc = initSeed;
	
	rCutGr = 3.;
	rrCutGr = Sqr (rCutGr);
	rCutHS = 1.;
	rrCutHS = Sqr (rCutHS);
	sigma = 1;
	//rCut = pow (2., 1./6.) * sigma;
	
	//rrCut = Sqr (rCut);
	//ri3Cut = 1. / (Cube (rCut));
	beta = 1. / (temperature);
	length = pow (nMol/rho,0.333333);
	rCut = length / 2.;
	rrCut = Sqr (rCut);
	//rri = 1. / rrCut;
	//rri3 = Cube (rri);
	//eCut = 4. *  rri3 * (rri3 - 1);
	eCorr = 2. * PI * rho * sigma * epsilon * ((kD * rCut +1.)/(kD*kD)) * exp (-kD * sigma * ((rCut / sigma)-1.));
	pCorr = (2./3.) * PI * rho * rho * sigma * epsilon * ((kD * kD * rrCut + 3. * kD * rCut +3.)/(kD*kD)) * exp (-kD * sigma * ((rCut / sigma)-1.));
	DkeCorr = 2. * PI * rho * sigma * epsilon * exp (-kD * sigma * ((rCut / sigma)-1.)) * ((rCut/(kD*kD)) - (2. * (kD*rCut+1.)/(kD*kD*kD)) + ((1.-rCut)*(kD*rCut+1.)/(kD*kD)));
	
	Ucell = round (pow(nMol/4.,1./3.));
	lengthFcc = (1. / pow (rhoFcc / 4., 1./3.)) * Ucell;
	//printf("%d\t\t%lf\n",Ucell,lengthFcc);
	rCutFcc = lengthFcc / 2.;
	rrCutFcc = Sqr (rCutFcc);
	//rri = 1. / rrCutFcc;
	//rri3 = Cube (rri);
	//eCut = 4. *  rri3 * (rri3 - 1);
	eCorrFcc = 2. * PI * rhoFcc * sigma * epsilon * ((kD * rCutFcc +1.)/(kD*kD)) * exp (-kD * sigma * ((rCutFcc / sigma)-1.));
	pCorrFcc = (2./3.) * PI * rhoFcc * rhoFcc * sigma * epsilon * ((kD * kD * rrCutFcc + 3. * kD * rCutFcc +3.)/(kD*kD)) * exp (-kD * sigma * ((rCutFcc / sigma)-1.));
	DkeCorrFcc = 2. * PI * rhoFcc * sigma * epsilon * exp (-kD * sigma * ((rCutFcc / sigma)-1.)) * ((rCutFcc/(kD*kD)) - (2. * (kD*rCutFcc+1.)/(kD*kD*kD)) + ((1.-rCutFcc)*(kD*rCutFcc+1.)/(kD*kD)));
	
}


//#####################################
void SetupJob ()
{
	int n;
	//AllocArrays ();
	stepCount = 0;
	sampleCount = 0;
	//For Fluid
	nAcc = 0;
	nTransAcc = 0;
	nTransAtt = 0;
	nVolAcc = 0;
	nVolAtt = 0;
	eSum = 0.0;
	rhoSum = 0.0;
	rhoSum1 = 0.0;
	//pSum = 0.0;
	//ngr = 0;  
	InitCoords ();
	potEnergy ();
	
	//For Fcc
	nAccFcc = 0;
	nTransAccFcc = 0;
	nTransAttFcc = 0;
	nVolAccFcc = 0;
	nVolAttFcc = 0;
	eSumFcc = 0.0;
	rhoSumFcc = 0.0;
	rhoSumFcc1 = 0.0;
	//pSum = 0.0;
	//ngr = 0;  
	InitCoordsFcc ();
	potEnergyFcc ();
}


//#####################################
void AllocArrays ()
{
	int k;
	AllocMem (mol, nMol, Mol);
	AllocMem (molFcc, nMol, Mol);
	//AllocMem (gr, nHist, real);
	//AllocMem (grArray, nHist, real);
}


//#########################################
void monteCarloNPT ( )
{
	int i;
	if(fmod(stepCount,7)==0)	{
		if(nTransAcc*(1.0/nTransAtt)<0.5)	deltaR=deltaR*0.95;
		else	deltaR=deltaR*1.05;
		nTransAcc=0;
		nTransAtt=0;
	}
	if(fmod(stepCount,nMol)==0) 	{
		if(nVolAcc*(1.0/nVolAtt)<0.5)	deltalnV=deltalnV*0.95;
		else	deltalnV=deltalnV*1.05;
		nVolAcc=0;
		nVolAtt=0;
	}
	
	for (i = 0; i <= nMol; i++) {
		molIndex = ran2(&seed) * (nMol + 1);
		//printf ("%d\n",molIndex);
		if (molIndex < nMol)	{
			nTransAtt++;
			monteCarloMove ();
		}
		else	{
			nVolAtt++;
			monteCarloVol ();
		}
	}
}


//#########################################
void monteCarloNPTFcc ( )
{
	int i;
	if(fmod(stepCount,7)==0)	{
		if(nTransAccFcc*(1.0/nTransAttFcc)<0.5)	deltaRFcc=deltaRFcc*0.95;
		else	deltaRFcc=deltaRFcc*1.05;
		nTransAccFcc=0;
		nTransAttFcc=0;
	}
	if(fmod(stepCount,nMol)==0) 	{
		if(nVolAccFcc*(1.0/nVolAttFcc)<0.5)	deltalnVFcc=deltalnVFcc*0.95;
		else	deltalnVFcc=deltalnVFcc*1.05;
		nVolAccFcc=0;
		nVolAttFcc=0;
	}
	
	for (i = 0; i <= nMol; i++) {
		molIndex = ran2(&seedFcc) * (nMol + 1);
		//printf ("%d\n",molIndex);
		if (molIndex < nMol)	{
			nTransAttFcc++;
			monteCarloMoveFcc ();
		}
		else	{
			nVolAttFcc++;
			monteCarloVolFcc ();
		}
	}
}



//#########################################
void monteCarloMove ( )
{
	int i;
	VecR dr, rOld, randNumShifted;
	real energyOld, virOld, randNum, deltaE, bdE;
	real s1, s2, s3;
	nTransAcc++;
	potEnergyPerAtom (molIndex);
	energyOld = energy;
	virOld = vir;
	
	s1 = (ran2(&seed) - 0.5);
	s2 = (ran2(&seed) - 0.5);
	s3 = (ran2(&seed) - 0.5);
	VSet(randNumShifted, s1, s2, s3); 
	//randNumShifted.x = (ran2(&seed) - 0.5);
	//randNumShifted.y = (ran2(&seed) - 0.5);
	//randNumShifted.z = (ran2(&seed) - 0.5);
	
	VSCopy(dr, deltaR, randNumShifted);
	//dr.x = deltaR * (ran2(&seed) - 0.5);
	//dr.y = deltaR * (ran2(&seed) - 0.5);
	//dr.z = deltaR * (ran2(&seed) - 0.5);
	
	VCopy(rOld, mol[molIndex].r);
	//rOld.x = mol[molIndex].r.x;
	//rOld.y = mol[molIndex].r.y;
	//rOld.z = mol[molIndex].r.z;
	
	VVAdd(mol[molIndex].r, dr);
	//mol[molIndex].r.x += dr.x;
	//mol[molIndex].r.y += dr.y;
	//mol[molIndex].r.z += dr.z;
	
	ApplyPeriodicBoundaryCond ();
	
	potEnergyPerAtom (molIndex);
	deltaE = energy - energyOld;
	bdE = beta * deltaE;
	randNum = ran2(&seed);
	if ((randNum > exp (-bdE)) || (Flag == 0)) {
		VCopy(mol[molIndex].r, rOld);
		//mol[molIndex].r.x = rOld.x;
		//mol[molIndex].r.y = rOld.y;
		//mol[molIndex].r.z = rOld.z;
		energy = energyOld;
		vir = virOld;
		nTransAcc--;
		
	}
}

//#########################################
void monteCarloMoveFcc ( )
{
	int i;
	VecR dr, rOld, randNumShifted;
	real energyOld, virOld, randNum, deltaE, bdE;
	real s1, s2, s3;
	nTransAccFcc++;
	potEnergyPerAtomFcc (molIndex);
	energyOld = energyFcc;
	virOld = virFcc;
	
	s1 = (ran2(&seedFcc) - 0.5);
	s2 = (ran2(&seedFcc) - 0.5);
	s3 = (ran2(&seedFcc) - 0.5);
	VSet(randNumShifted, s1, s2, s3); 
	//randNumShifted.x = (ran2(&seed) - 0.5);
	//randNumShifted.y = (ran2(&seed) - 0.5);
	//randNumShifted.z = (ran2(&seed) - 0.5);
	
	VSCopy(dr, deltaRFcc, randNumShifted);
	//dr.x = deltaRFcc * (ran2(&seed) - 0.5);
	//dr.y = deltaRFcc * (ran2(&seed) - 0.5);
	//dr.z = deltaRFcc * (ran2(&seed) - 0.5);
	
	VCopy(rOld, molFcc[molIndex].r);
	//rOld.x = molFcc[molIndex].r.x;
	//rOld.y = molFcc[molIndex].r.y;
	//rOld.z = molFcc[molIndex].r.z;
	
	VVAdd(molFcc[molIndex].r, dr);
	//molFcc[molIndex].r.x += dr.x;
	//molFcc[molIndex].r.y += dr.y;
	//molFcc[molIndex].r.z += dr.z;
	
	ApplyPeriodicBoundaryCondFcc ();
	
	potEnergyPerAtomFcc (molIndex);
	deltaE = energyFcc - energyOld;
	bdE = beta * deltaE;
	randNum = ran2(&seedFcc);
	if ((randNum > exp (-bdE)) || (Flag == 0)) {
		VCopy(molFcc[molIndex].r, rOld);
		//molFcc[molIndex].r.x = rOld.x;
		//molFcc[molIndex].r.y = rOld.y;
		//molFcc[molIndex].r.z = rOld.z;
		energyFcc = energyOld;
		virFcc = virOld;
		nTransAccFcc--;
		
	}
}


//#########################################
void monteCarloVol ( )
{
	int i;
	VecR dr, rOld, randNumShifted;
	real energyOld, virOld, randNum, deltaE, bdE, bPdV, divlnV, arg;
	real V0, V, scaleFac, lnV, lnV0, newLength;
	nVolAcc++;
	potEnergy ();
	energyOld = energy;
	virOld = vir;
	
	V0 = Cube (length);
	lnV0 = log (V0);
	lnV = lnV0 + deltalnV * (ran2(&seed) - 0.5);
	V=exp(lnV);
	
	newLength = pow(V,0.333333333);
	scaleFac = newLength / length;
	
	for (i = 0; i < nMol; i++) {
		VScale(mol[i].r, scaleFac);
		//mol[i].r.x *= scaleFac;
		//mol[i].r.y *= scaleFac;
		//mol[i].r.z *= scaleFac;
	}
	length = newLength;
	
	rCut *= scaleFac;
	rrCut = Sqr (rCut);
	rho = nMol / (V);
	//ri3Cut = 1. / (Cube (rCut));
	//rri = 1. / rrCut;
	//rri3 = Cube (rri);
	//eCut = 4. *  rri3 * (rri3 - 1);
	eCorr = 2. * PI * rho * sigma * epsilon * ((kD * rCut +1.)/(kD*kD)) * exp (-kD * sigma * ((rCut / sigma)-1.));
	pCorr = (2./3.) * PI * rho * rho * sigma * epsilon * ((kD * kD * rrCut + 3. * kD * rCut +3.)/(kD*kD)) * exp (-kD * sigma * ((rCut / sigma)-1.));
	DkeCorr = 2. * PI * rho * sigma * epsilon * exp (-kD * sigma * ((rCut / sigma)-1.)) * ((rCut/(kD*kD)) - (2. * (kD*rCut+1.)/(kD*kD*kD)) + ((1.-rCut)*(kD*rCut+1.)/(kD*kD)));
	
	potEnergy ();
	deltaE = (energy - energyOld);
	bdE = beta * deltaE;
	bPdV = pressure * (V-V0) * beta;
	divlnV = lnV - lnV0;
	arg = -1. * (bdE + bPdV - ((nMol + 1) * divlnV));
	randNum = ran2(&seed);
	
	if ((randNum > exp (arg)) || (Flag == 0)) {
		scaleFac = 1. / scaleFac;
		
		for (i = 0; i < nMol; i++) {
			VScale(mol[i].r, scaleFac);
			//mol[molIndex].r.x *= scaleFac;
			//mol[molIndex].r.y *= scaleFac;
			//mol[molIndex].r.z *= scaleFac;
		}
		length *= scaleFac;
		
		rCut *= scaleFac;
		rrCut = Sqr (rCut);
		rho = nMol / (V0);
		//ri3Cut = 1. / (Cube (rCut));
		//rri = 1. / rrCut;
		//rri3 = Cube (rri);
		//eCut = 4. *  rri3 * (rri3 - 1);
		eCorr = 2. * PI * rho * sigma * epsilon * ((kD * rCut +1.)/(kD*kD)) * exp (-kD * sigma * ((rCut / sigma)-1.));
		pCorr = (2./3.) * PI * rho * rho * sigma * epsilon * ((kD * kD * rrCut + 3. * kD * rCut +3.)/(kD*kD)) * exp (-kD * sigma * ((rCut / sigma)-1.));
		DkeCorr = 2. * PI * rho * sigma * epsilon * exp (-kD * sigma * ((rCut / sigma)-1.)) * ((rCut/(kD*kD)) - (2. * (kD*rCut+1.)/(kD*kD*kD)) + ((1.-rCut)*(kD*rCut+1.)/(kD*kD)));
		
		energy = energyOld;
		vir = virOld;
		nVolAcc--;
	}
}


//#########################################
void monteCarloVolFcc ( )
{
	int i;
	VecR dr, rOld, randNumShifted;
	real energyOld, virOld, randNum, deltaE, bdE, bPdV, divlnV, arg;
	real V0, V, scaleFac, lnV, lnV0, newLength;
	nVolAccFcc++;
	potEnergyFcc ();
	energyOld = energyFcc;
	virOld = virFcc;
	
	V0 = Cube (lengthFcc);
	lnV0 = log (V0);
	lnV = lnV0 + deltalnVFcc * (ran2(&seedFcc) - 0.5);
	V=exp(lnV);
	
	newLength = pow(V,0.333333333);
	scaleFac = newLength / lengthFcc;
	
	for (i = 0; i < nMol; i++) {
		VScale(molFcc[i].r, scaleFac);
		//molFcc[i].r.x *= scaleFac;
		//molFcc[i].r.y *= scaleFac;
		//molFcc[i].r.z *= scaleFac;
	}
	lengthFcc = newLength;
	
	rCutFcc *= scaleFac;
	rrCutFcc = Sqr (rCutFcc);
	rhoFcc = nMol / (V);
	//ri3Cut = 1. / (Cube (rCutFcc));
	//rri = 1. / rrCutFcc;
	//rri3 = Cube (rri);
	//eCut = 4. *  rri3 * (rri3 - 1);
	eCorrFcc = 2. * PI * rhoFcc * sigma * epsilon * ((kD * rCutFcc +1.)/(kD*kD)) * exp (-kD * sigma * ((rCutFcc / sigma)-1.));
	pCorrFcc = (2./3.) * PI * rhoFcc * rhoFcc * sigma * epsilon * ((kD * kD * rrCutFcc + 3. * kD * rCutFcc +3.)/(kD*kD)) * exp (-kD * sigma * ((rCutFcc / sigma)-1.));
	DkeCorrFcc = 2. * PI * rhoFcc * sigma * epsilon * exp (-kD * sigma * ((rCutFcc / sigma)-1.)) * ((rCutFcc/(kD*kD)) - (2. * (kD*rCutFcc+1.)/(kD*kD*kD)) + ((1.-rCutFcc)*(kD*rCutFcc+1.)/(kD*kD)));
	
	potEnergyFcc ();
	deltaE = (energyFcc - energyOld);
	bdE = beta * deltaE;
	bPdV = pressure * (V-V0) * beta;
	divlnV = lnV - lnV0;
	arg = -1. * (bdE + bPdV - ((nMol + 1) * divlnV));
	randNum = ran2(&seedFcc);
	
	if ((randNum > exp (arg)) || (Flag == 0)) {
		scaleFac = 1. / scaleFac;
		
		for (i = 0; i < nMol; i++) {
			VScale(molFcc[i].r, scaleFac);
			//molFcc[molIndex].r.x *= scaleFac;
			//molFcc[molIndex].r.y *= scaleFac;
			//molFcc[molIndex].r.z *= scaleFac;
		}
		lengthFcc *= scaleFac;
		
		rCutFcc *= scaleFac;
		rrCutFcc = Sqr (rCutFcc);
		rhoFcc = nMol / (V0);
		//ri3Cut = 1. / (Cube (rCutFcc));
		//rri = 1. / rrCutFcc;
		//rri3 = Cube (rri);
		//eCut = 4. *  rri3 * (rri3 - 1);
		eCorrFcc = 2. * PI * rhoFcc * sigma * epsilon * ((kD * rCutFcc +1.)/(kD*kD)) * exp (-kD * sigma * ((rCutFcc / sigma)-1.));
		pCorrFcc = (2./3.) * PI * rhoFcc * rhoFcc * sigma * epsilon * ((kD * kD * rrCutFcc + 3. * kD * rCutFcc +3.)/(kD*kD)) * exp (-kD * sigma * ((rCutFcc / sigma)-1.));
		DkeCorrFcc = 2. * PI * rhoFcc * sigma * epsilon * exp (-kD * sigma * ((rCutFcc / sigma)-1.)) * ((rCutFcc/(kD*kD)) - (2. * (kD*rCutFcc+1.)/(kD*kD*kD)) + ((1.-rCutFcc)*(kD*rCutFcc+1.)/(kD*kD)));
		
		energyFcc = energyOld;
		virFcc = virOld;
		nVolAccFcc--;
	}
}


//#########################################
int potEnergy ( )
{
	VecR dr;
	real rr, r;
	int j1, j2, n;
	
	energy = 0.;
	vir = 0.;
	DkEn = 0.;
	
	//rrCut = Sqr (rCut);
	
	// Interaction between monomers
	for (j1 = 0; j1 < nMol - 1; j1 ++) {
		// Excluded volume interaction between monomers
		for (j2 = j1 + 1; j2 < nMol; j2 ++) {
			VSub (dr, mol[j1].r, mol[j2].r);
			if (dr.x >= 0.5 * length)      dr.x -= length;
			else if (dr.x < -0.5 * length) dr.x += length;
			if (dr.y >= 0.5 * length)      dr.y -= length;
			else if (dr.y < -0.5 * length) dr.y += length;
			if (dr.z >= 0.5 * length)      dr.z -= length;
			else if (dr.z < -0.5 * length) dr.z += length;
			
			//VWrapAll (dr);
			rr = VLenSq (dr);
			r = sqrt (rr);
			if (r < rCutHS) {
				Flag = 0;
				return 0;
			}
			else {
				Flag = 1;
				if (rr < rrCut) {
					//rri = 1. / rr;
					//rri3 = Cube (rri);
					energy += (epsilon * exp (-kD * sigma * ((r / sigma)-1.))) / (r / sigma);
					vir += ((epsilon * exp (-kD * sigma * ((r / sigma)-1.))) / (r / sigma)) * ((r * kD) + 1.);
					DkEn += (epsilon * exp (-kD * sigma * ((r / sigma)-1.)) * (sigma - r)) / r;
				}
			}
		}
	}
	energy += nMol * eCorr;
	//DkEn += nMol * DkeCorr;
	return 0;
}


//#########################################
int potEnergyFcc ( )
{
	VecR dr;
	real rr, r;
	int j1, j2, n;
	
	energyFcc = 0.;
	virFcc = 0.;
	DkEnFcc = 0.;
	
	//rrCutFcc = Sqr (rCutFcc);
	
	// Interaction between monomers
	for (j1 = 0; j1 < nMol - 1; j1 ++) {
		// Excluded volume interaction between monomers
		for (j2 = j1 + 1; j2 < nMol; j2 ++) {
			VSub (dr, molFcc[j1].r, molFcc[j2].r);
			if (dr.x >= 0.5 * lengthFcc)      dr.x -= lengthFcc;
			else if (dr.x < -0.5 * lengthFcc) dr.x += lengthFcc;
			if (dr.y >= 0.5 * lengthFcc)      dr.y -= lengthFcc;
			else if (dr.y < -0.5 * lengthFcc) dr.y += lengthFcc;
			if (dr.z >= 0.5 * lengthFcc)      dr.z -= lengthFcc;
			else if (dr.z < -0.5 * lengthFcc) dr.z += lengthFcc;
			
			//VWrapAll (dr);
			rr = VLenSq (dr);
			r = sqrt (rr);
			if (r < rCutHS) {
				Flag = 0;
				return 0;
			}
			else {
				Flag = 1;
				if (rr < rrCutFcc) {
					//rri = 1. / rr;
					//rri3 = Cube (rri);
					energyFcc += (epsilon * exp (-kD * sigma * ((r / sigma)-1.))) / (r / sigma);
					virFcc += ((epsilon * exp (-kD * sigma * ((r / sigma)-1.))) / (r / sigma)) * ((r * kD) + 1.);
					DkEnFcc += (epsilon * exp (-kD * sigma * ((r / sigma)-1.)) * (sigma - r)) / r;
				}
			}
		}
	}
	energyFcc += nMol * eCorrFcc;
	//DkEnFcc += nMol * DkeCorrFcc;
	return 0;
}


//#########################################
int potEnergyPerAtom (int j2)
{
	VecR dr;
	real rr, r;
	int j1;
	
	energy = 0.;
	vir = 0.;	
	// Interaction between monomers
	for (j1 = 0; j1 < nMol; j1 ++) {
		// Excluded volume interaction between monomers
		if (j1 != j2) {
			VSub (dr, mol[j1].r, mol[j2].r);
			if (dr.x >= 0.5 * length)      dr.x -= length;
			else if (dr.x < -0.5 * length) dr.x += length;
			if (dr.y >= 0.5 * length)      dr.y -= length;
			else if (dr.y < -0.5 * length) dr.y += length;
			if (dr.z >= 0.5 * length)      dr.z -= length;
			else if (dr.z < -0.5 * length) dr.z += length;
			
			//VWrapAll (dr);
			rr = VLenSq (dr);
			r = sqrt (rr);
			if (r < rCutHS) {
				Flag = 0;
				return 0;
			}
			else {
				Flag = 1;
				if (rr < rrCut) {
					//rri = 1. / rr;
					//rri3 = Cube (rri);
					energy += (epsilon * exp (-kD * sigma * ((r / sigma)-1.))) / (r / sigma);
					vir += ((epsilon * exp (-kD * sigma * ((r / sigma)-1.))) / (r / sigma)) * ((r * kD) + 1.);
				}
			}
		}
	}
	energy += nMol * eCorr;
	return 0;
}


//#########################################
int potEnergyPerAtomFcc (int j2)
{
	VecR dr;
	real rr, r;
	int j1;
	
	energyFcc = 0.;
	virFcc = 0.;	
	// Interaction between monomers
	for (j1 = 0; j1 < nMol; j1 ++) {
		// Excluded volume interaction between monomers
		if (j1 != j2) {
			VSub (dr, molFcc[j1].r, molFcc[j2].r);
			if (dr.x >= 0.5 * lengthFcc)      dr.x -= lengthFcc;
			else if (dr.x < -0.5 * lengthFcc) dr.x += lengthFcc;
			if (dr.y >= 0.5 * lengthFcc)      dr.y -= lengthFcc;
			else if (dr.y < -0.5 * lengthFcc) dr.y += lengthFcc;
			if (dr.z >= 0.5 * lengthFcc)      dr.z -= lengthFcc;
			else if (dr.z < -0.5 * lengthFcc) dr.z += lengthFcc;
			
			//VWrapAll (dr);
			rr = VLenSq (dr);
			r = sqrt (rr);
			if (r < rCutHS) {
				Flag = 0;
				return 0;
			}
			else {
				Flag = 1;
				if (rr < rrCutFcc) {
					//rri = 1. / rr;
					//rri3 = Cube (rri);
					energyFcc += (epsilon * exp (-kD * sigma * ((r / sigma)-1.))) / (r / sigma);
					virFcc += ((epsilon * exp (-kD * sigma * ((r / sigma)-1.))) / (r / sigma)) * ((r * kD) + 1.);
				}
			}
		}
	}
	energyFcc += nMol * eCorrFcc;
	return 0;
}

//#########################################
void CalcGr ()
{
	VecR dr;
	real rr, r, rLeft, rRight, nIdeal;
	int n, j1, j2, bin, d;
	rhoSum1 += nMol / (Cube (length));
	if (ngr == 0) {
		for (n = 0; n < nHist; n ++) {
			gr[n] = 0.;
			grArray[n] = 0.;
		}
	}
	
	//if (flag == 0) {
	for (j1 = 0; j1 < nMol - 1; j1 ++) {
		for (j2 = j1 + 1; j2 < nMol; j2 ++) {
			VSub (dr, mol[j1].r, mol[j2].r);
			if (dr.x >= 0.5 * length)      dr.x -= length;
			else if (dr.x < -0.5 * length) dr.x += length;
			if (dr.y >= 0.5 * length)      dr.y -= length;
			else if (dr.y < -0.5 * length) dr.y += length;
			if (dr.z >= 0.5 * length)      dr.z -= length;
			else if (dr.z < -0.5 * length) dr.z += length;
			
			//VWrapAll (dr);
			rr = VLenSq (dr);
			r = sqrt (rr);
			if (rr < rrCutGr) {
				bin = (int) (r / binWidth);
				//printf("%d\n",bin);
				gr[bin] += 2; 
				
			}
		}
	}
	//}
	++ ngr;
	if (stepCount == limitGr) {
		for(n = 0; n < nHist; n++) {
			//r = binWidth * (n);
			
			
			rLeft = n * binWidth;
			rRight = (n + 1) * binWidth;
			nIdeal = consttFact * (rhoSum1/ngr) * (Cube(rRight)-Cube(rLeft));
			grArray[n] = (gr[n]) / (ngr * nMol * nIdeal);
		}
		PrintPropsGrToFile (fp1);
		
		d = (int) (rCutHS / binWidth);
		rLeft = d * binWidth;
		rRight = (d + 1) * binWidth;
		
		nIdeal = consttFact * (rhoSum1/ngr) * (Cube(rRight)-Cube(rLeft));
		
		grpTerm = gr[d] /  (ngr * nMol * nIdeal);
		ngr = 0;
		rhoSum1 =0.;
	}
}

//#########################################
void ApplyPeriodicBoundaryCond ()
{
	if (mol[molIndex].r.x > length)      mol[molIndex].r.x -= length;
	else if (mol[molIndex].r.x < 0) mol[molIndex].r.x += length;
	if (mol[molIndex].r.y > length)      mol[molIndex].r.y -= length;
	else if (mol[molIndex].r.y < 0) mol[molIndex].r.y += length;
	if (mol[molIndex].r.z > length)      mol[molIndex].r.z -= length;
	else if (mol[molIndex].r.z < 0) mol[molIndex].r.z += length;
}


//#########################################
void ApplyPeriodicBoundaryCondFcc ()
{
	if (molFcc[molIndex].r.x > lengthFcc)      molFcc[molIndex].r.x -= lengthFcc;
	else if (molFcc[molIndex].r.x < 0) molFcc[molIndex].r.x += lengthFcc;
	if (molFcc[molIndex].r.y > lengthFcc)      molFcc[molIndex].r.y -= lengthFcc;
	else if (molFcc[molIndex].r.y < 0) molFcc[molIndex].r.y += lengthFcc;
	if (molFcc[molIndex].r.z > lengthFcc)      molFcc[molIndex].r.z -= lengthFcc;
	else if (molFcc[molIndex].r.z < 0) molFcc[molIndex].r.z += lengthFcc;
}


//#########################################
void InitCoords ()
{
	//VecR c, gap;
	int n, nx, ny, nz, i, j, k;
	real s1, s2, s3;
	
	
	//**********************intialize Particle Position***********************
	
	nx=0;ny=0;nz=0;
	n = 2;
	while ((n*n*n)<nMol) n++;
	for (i=0; i < nMol; i++) {
		s1 = ((double)nx+0.5) * length / n;
		s2 = ((double)ny+0.5) * length / n;
		s3 = ((double)nz+0.5) * length / n;
		VSet(mol[i].r, s1, s2, s3); 
		//mol[i].r.x = ((double)nx+0.5) * length / n;
		//mol[i].r.y = ((double)ny+0.5) * length / n;
		//mol[i].r.z = ((double)nz+0.5) * length / n;
		nx++;
		if (nx == n) {
			nx = 0;
			ny++;
			if (ny == n) {
				ny = 0;
				nz++;
			}
		}
	}
}


//#########################################
void InitCoordsFcc ()
{
	VecR c, gap;
	int n, nx, ny, nz, i, j, k;
	real s1, s2, s3;
	
	
	//**********************intialize Particle Position**********************
	
	gap.x = lengthFcc /(double) Ucell;
	gap.y = gap.x;
	gap.z = gap.x;
	
	region.x = lengthFcc;
	region.y = lengthFcc;
	region.z = lengthFcc;
	//printf("%lf\t\t%lf\n",gap.x,gap.y);
	//printf("%lf\t\t%lf\n",region.x,region.y);
	n = 0;
	for (nz = 0; nz < Ucell; nz ++) {
		for (ny = 0; ny < Ucell; ny ++) {
			for (nx = 0; nx < Ucell; nx ++) {
				VSet (c, nx + 0.25, ny + 0.25, nz + 0.25);
				VMul (c, c, gap);
				//VVSAdd (c, -0.5, region);
				for (j = 0; j < 4; j ++) {
					molFcc[n].r = c;
					if (j != 3) {
						if (j != 0) molFcc[n].r.x += 0.5 * gap.x;
						if (j != 1) molFcc[n].r.y += 0.5 * gap.y;
						if (j != 2) molFcc[n].r.z += 0.5 * gap.z;
					}
					++ n;
				}
			}
		}
	}  
	//printf("%d\n",n);
	//PrintChainConfig (fp3);
	
}


//#########################################
void EvalProps ()
{
	potEnergy ();
	potEnergyFcc ();
	eSum += DkEn;
	eSumFcc += DkEnFcc;
	//pSum += ((1. / 3.) * vir) / (Cube (length)) + pCorr;
	rhoSum += nMol / (Cube (length));
	rhoSumFcc += nMol / (Cube (lengthFcc));
	
	rhoSum1 += (Cube (length)) / nMol;
	rhoSumFcc1 += (Cube (lengthFcc)) / nMol;
}


//#########################################
/*void PrintChainProps (FILE *fp)
 * {
 * int i;
 * for(i = 0 ; i < tBins ; i++)
 * 	fprintf(fp,"%e %e\n",i * deltaT, velCorr[i] / velCorr[0]);
 *	fflush (fp);
 * }*/

//#########################################
/*void PrintChainConfig (FILE *fp)
 * {
 *	int i;
 *	for (i = 0; i < chainLen; i ++)
 *		fprintf (fp, "%f %f\n", chain[i].r.x, chain[i].r.y);
 *	//for (i = 1; i < chainLen; i += 2)
 *		//fprintf (fp, "%f %f %d %f %f %f %f\n", chain[i].r.x, chain[i].r.y, 1, chain[i].rv.x, chain[i].rv.y, chain[i].ra.x, chain[i].ra.y);
 *	//fprintf (fp, "\n\n");
 *	fflush (fp);
 * }*/

//#########################################
void PrintChainConfig (FILE *fp)
{
	int i;
	for (i = 0; i < nMol; i ++)
		fprintf (fp, "%lf %lf %lf\n", molFcc[i].r.x, molFcc[i].r.y, molFcc[i].r.z);
	fflush (fp);
}


//#########################################
void PrintPropsSTDOUT ()
{
	fprintf(stdout,
			"Number of particles=\t\t %i\n" "Temperature=\t\t %.5lf\n" "Pressure=\t\t %.5lf\n" "Average Energy=\t\t %.5lf\n" "Density=\t\t %.5lf\n" "Simulated Pressure=\t\t %.5lf\n" "Length=\t\t %.5lf\n",nMol, temperature, pressure, eSum, rhoSum, simulatedPress, length);
}


//#########################################
/*void PrintPropsToFile (FILE *fp)
 * {
 * fprintf(fp,
 *	    "Number of particles=\t\t %i\n" "Temperature=\t\t %.5lf\n" "Pressure=\t\t %.5lf\n" "Average Energy=\t\t %.5lf\n" "Density=\t\t %.5lf\n" "Simulated Pressure=\t\t %.5lf\n" "Simulated Pressure G(r)=\t\t %.5lf\n" "Length=\t\t %.5lf\n""Displ attempts=\t\t%i\n""Vol change attempts=\t\t%i\n""Acceptance ratio of displ.=\t\t%.5lf\n""Acceptance ratio of vol moves=\t\t%.5lf\n""Total acceptance ratio=\t\t%.5lf\n",nMol, temperature, pressure, eSum, rhoSum, simulatedPress, grPressure, length,nTransAtt,nVolAtt,((double)nTransAcc)/(nTransAtt),((double)nVolAcc)/(nVolAtt),((double)(nTransAcc+nVolAcc))/(nTransAtt+nVolAtt));
 * }*/

//#########################################
void PrintPropsToFile (FILE *fp)
{
	fprintf(fp,
			"%lf \t\t %lf \t\t %lf \t\t %lf \t\t %lf \t\t %lf \t\t %lf\n",kDInv, kD, pressure, rhoSum, rhoSumFcc, phi, phiFcc);
}



//#########################################
void PrintPropsGrToFile (FILE *fp)
{
	real r;
	int n;
	
	for (n = 0; n < nHist; n ++) {
		r = n * binWidth;
		fprintf (fp, "%8.4f \t %8.4f\n", r, grArray[n]);
	}
	fprintf (fp, "\n\n");
	fflush (fp);
}


#include "gasdev.c"
#include "ran2.c"
#include "gamdev.c"
#include "in_rand.c"
#include "in_errexit.c"
//#include "in_namelist.c"
#include "maxboltzdev.c"
